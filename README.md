# Rendering Templates

This repository contains rendering templates used in our platform. 
These templates help you to create your own templates and use them in our platform. 
This way you can customize every interaction and communication to your individual needs.

## Using a template 

In order to use a custom template you have to add it's sourcecode in our platform.

1. Goto `Public Website -> Message Templates -> New`
2. Fill in the required details:
  * for `Service` choose the first subdirectory in this repositoy.
  * for `Template Name` set the remaining path of the template from this repository without extension
  * for E-Mails Subject and Text content are relevant, for PDF the fields can be omitted
3. Save the tempalte 
4. If you want to add Translations you can now do so in the detail view of the newly created template

## Example

To overwrite the tempalte in `mail/notification/application/application_accepted.de.html.jinja2` you have to choose
* **Service**: `Notification`
* **Template Name**: `application/application_accepted`

